#include QMK_KEYBOARD_H
#include "bootloader.h"
#ifdef PROTOCOL_LUFA
#include "lufa.h"
#include "split_util.h"
#endif
#ifdef SSD1306OLED
  #include "ssd1306.h"
#endif
#include "../../../../../quantum/keymap_extras/keymap_jp.h"

extern keymap_config_t keymap_config;

#ifdef RGBLIGHT_ENABLE
//Following line allows macro to read current RGB settings
extern rgblight_config_t rgblight_config;
#endif

extern uint8_t is_master;

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
enum layer_number {
    _COLEMAK = 0,
    _CURSOR,
    _MOUSE,
    _NUMBER,
    _DOUBLE,
    _ADJUST
};

enum custom_keycodes {
  JP = SAFE_RANGE,
  US,
  NUMBER,
  ADJUST,
  RGBRST,
  ZHTG,
  CIRC,
  AT,
  LBRC,
  COLN,
  RBRC,
  BSLS,
  DQT,
  AMPR,
  QUOT,
  LPRN,
  RPRN,
  EQL,
  TILD,
  PIPE,
  GRV,
  LCBR,
  PLUS,
  ASTR,
  RCBR,
  UNDS,
  MINS,
  SCLN,
  COMM,
  DOT,
  SLSH,
  EXLM,
  HASH,
  DLR,
  PERC,
  LT,
  GT,
  QUES
};

enum macro_keycodes {
  KC_SAMPLEMACRO,
};


// Fillers to make layering more clear
#define _______ KC_TRNS
#define XXXXXXX KC_NO
#define PREVWSP G(KC_PGUP)
#define NEXTWSP G(KC_PGDN)
#define CURSOR MO(_CURSOR)
#define MOUSE MO(_MOUSE)
#define TMUX    LCTL(KC_B)
#define GUISPCE LGUI(KC_SPACE)
#define SLEP    LGUI(LCTL(KC_Q))
#define TERM    LGUI(LSFT(KC_ENTER))
#define FULL    LGUI(KC_F)
#define CLOSE LGUI(LSFT(KC_C))
#define LSTAB LSFT(KC_TAB)
//Macros
#define M_SAMPLE M(KC_SAMPLEMACRO)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_COLEMAK] = LAYOUT( \
      KC_Q,    KC_W,    KC_F,    KC_P,    KC_G,    GUISPCE,                   KC_ZKHK, KC_J,    KC_L,    KC_U,    KC_Y,    KC_SCLN, \
      KC_A,    KC_R,    KC_S,    KC_T,    KC_D,    PREVWSP,                   NEXTWSP, KC_H,    KC_N,    KC_E,    KC_I,    KC_O,    \
      KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    LSTAB  ,                   KC_TAB , KC_K,    KC_M,    KC_COMM, KC_DOT , KC_SLSH, \
      KC_LALT, TMUX   , KC_ESC , KC_BSPC, KC_LCTL, KC_ENT , KC_MHEN, KC_HENK, KC_ENT , KC_RCTL, KC_BSPC, KC_ESC , KC_UP,   KC_RALT, \
      ADJUST , MOUSE  , CURSOR , NUMBER , KC_LSFT, KC_SPC , KC_LGUI, KC_LGUI, KC_SPC , KC_RSFT, NUMBER , KC_LEFT, KC_DOWN, KC_RGHT  \
      ),

  [_CURSOR] =  LAYOUT( \
      _______, _______, _______, _______, _______, _______,                   _______, _______, _______, KC_UP  , _______, _______, \
      _______, _______, _______, _______, _______, _______,                   _______, _______, KC_LEFT, KC_DOWN, KC_RGHT, _______, \
      _______, _______, _______, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
      _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
      _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______  \
      ),

  [_MOUSE] =  LAYOUT( \
      _______, _______, _______, _______, _______, _______,                   _______, KC_WH_U, KC_BTN1, KC_MS_U, KC_BTN2, _______, \
      _______, _______, _______, _______, _______, _______,                   _______, KC_WH_D, KC_MS_L, KC_MS_D, KC_MS_R, _______, \
      _______, _______, _______, _______, _______, _______,                   _______, _______, KC_WH_L, KC_WH_R, _______, _______, \
      _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
      _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______  \
      ),

  [_NUMBER] = LAYOUT( \
      KC_EXLM, AT     , KC_HASH, KC_DLR , KC_PERC, _______,                   _______, CIRC   , AMPR   , ASTR   , KC_MINS, EQL    , \
      KC_1   , KC_2   , KC_3   , KC_4   , KC_5   , _______,                   _______, KC_6   , KC_7   , KC_8   , KC_9   , KC_0   , \
      GRV    , TILD   , LBRC   , LCBR   , LPRN   , KC_PGUP,                   KC_PGDN, RPRN   , RCBR   , RBRC   , UNDS   , PLUS   , \
      _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
      _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______  \
      ),

  [_DOUBLE] = LAYOUT( \
      _______, _______, KC_LT  , BSLS   , _______, _______,                   _______, _______, PIPE   , KC_GT  , _______, _______, \
      _______, GRV    , KC_EXLM, QUOT   , _______, _______,                   _______, _______, DQT    , KC_QUES, TILD   , _______, \
      _______, KC_COMM, KC_MINS, KC_SCLN, _______, _______,                   _______, _______, COLN   , EQL    , KC_DOT , _______, \
      KC_F1  , KC_F2  , KC_F3  , KC_F4  , KC_F5  , _______, _______, _______, _______, KC_F6  , KC_F7  , KC_F8  , KC_F9  , KC_F10 , \
      KC_F11 , KC_F12 , KC_F13 , _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______  \
      ),

  [_ADJUST] =  LAYOUT( \
      KC_BRID, KC_BRIU, _______, _______, _______, SLEP   ,                   KC_WAKE, _______, _______, _______, KC_VOLD, KC_VOLU, \
      KC_CAPS, KC_NLCK, KC_SLCK, _______, _______, _______,                   _______, _______, _______, _______, _______, _______, \
      _______, _______, _______, _______, _______, _______,                   RGB_M_P, RGB_M_B, _______, _______, _______, _______, \
      _______, _______, _______, _______, _______, _______, RESET  , KC_PWR , _______, _______, RGB_TOG, RGB_HUI, RGB_SAI, RGB_VAI, \
      _______, TERM   , FULL   , CLOSE  , _______, _______, US     , JP     , _______, RGBRST , RGB_MOD, RGB_HUD, RGB_SAD, RGB_VAD  \
      )
};

int NUMBER_PRESSED = 0;

#define JP_LAYOUT true
#define US_LAYOUT false
bool LAYOUT_STATUS = JP_LAYOUT;

// define variables for reactive RGB
bool TOG_STATUS = false;
int RGB_current_mode;

void persistent_default_layer_set(uint8_t layer) {
  uint16_t default_layer = 1UL<<layer;
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

#define KEY(CODE) (record->event.pressed ? SEND_STRING(SS_DOWN(X_ ## CODE)) : SEND_STRING(SS_UP(X_ ## CODE)))
#define KEY_S(CODE) \
  (record->event.pressed ? SEND_STRING(SS_DOWN(X_LSHIFT) SS_DOWN(X_ ## CODE)) : SEND_STRING(SS_UP(X_LSHIFT) SS_UP(X_ ## CODE)))
#define caseJP(CODE, US, JP) case CODE: (LAYOUT_STATUS == JP_LAYOUT ? JP : US); return false;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    caseJP(CIRC, KEY_S(6), KEY(EQUAL));
    caseJP(AT,   KEY_S(2), KEY(LBRACKET));
    caseJP(LBRC, KEY(LBRACKET), KEY(RBRACKET));
    caseJP(COLN, KEY_S(SCOLON), KEY(QUOTE));
    caseJP(RBRC, KEY(RBRACKET), KEY(NONUS_HASH));
    caseJP(BSLS, KEY(BSLASH), KEY(INT1));
    caseJP(DQT,  KEY_S(QUOTE), KEY_S(2));
    caseJP(AMPR, KEY_S(7), KEY_S(6));
    caseJP(QUOT, KEY(QUOTE), KEY_S(7));
    caseJP(LPRN, KEY_S(9), KEY_S(8));
    caseJP(RPRN, KEY_S(0), KEY_S(9));
    caseJP(EQL,  KEY(EQUAL), KEY_S(MINUS));
    caseJP(TILD, KEY_S(GRAVE), KEY_S(EQUAL));
    caseJP(PIPE, KEY_S(BSLASH), KEY_S(INT3));
    caseJP(GRV,  KEY(GRAVE), KEY_S(LBRACKET));
    caseJP(LCBR, KEY_S(LBRACKET), KEY_S(RBRACKET));
    caseJP(PLUS, KEY_S(EQUAL), KEY_S(SCOLON));
    caseJP(ASTR, KEY_S(8), KEY_S(QUOTE));
    caseJP(RCBR, KEY_S(RBRACKET), KEY_S(NONUS_HASH));
    caseJP(UNDS, KEY_S(MINUS), KEY_S(INT1));
    case JP:
      if (record->event.pressed) {
        LAYOUT_STATUS = JP_LAYOUT;
      }
      return false;
      break;
    case US:
      if (record->event.pressed) {
        LAYOUT_STATUS = US_LAYOUT;
      }
      return false;
      break;
    case NUMBER:
      if (record->event.pressed) {
        NUMBER_PRESSED ++;
        if (NUMBER_PRESSED == 1) {
          layer_on(_NUMBER);
        } else if (NUMBER_PRESSED == 2) {
          layer_on(_DOUBLE);
        }
      } else {
        NUMBER_PRESSED --;
        if (NUMBER_PRESSED == 0) {
          layer_off(_NUMBER);
        } else if (NUMBER_PRESSED == 1) {
          layer_off(_DOUBLE);
        }
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
      //led operations - RGB mode change now updates the RGB_current_mode to allow the right RGB mode to be set after reactive keys are released
    case RGB_MOD:
      #ifdef RGBLIGHT_ENABLE
        if (record->event.pressed) {
          rgblight_mode(RGB_current_mode);
          rgblight_step();
          RGB_current_mode = rgblight_config.mode;
        }
      #endif
      return false;
      break;
    case RGBRST:
      #ifdef RGBLIGHT_ENABLE
        if (record->event.pressed) {
          eeconfig_update_rgblight_default();
          rgblight_enable();
          rgblight_sethsv(343, 60, 255);
          RGB_current_mode = rgblight_config.mode;
        }
      #endif
      break;
  }
  return true;
}

void matrix_init_user(void) {
    #ifdef RGBLIGHT_ENABLE
      RGB_current_mode = rgblight_config.mode;
    #endif
    //SSD1306 OLED init, make sure to add #define SSD1306OLED in config.h
    #ifdef SSD1306OLED
        iota_gfx_init(!has_usb());   // turns on the display
    #endif
}

//SSD1306 OLED update loop, make sure to add #define SSD1306OLED in config.h
#ifdef SSD1306OLED

// hook point for 'led_test' keymap
//   'default' keymap's led_test_init() is empty function, do nothing
//   'led_test' keymap's led_test_init() force rgblight_mode_noeeprom(RGBLIGHT_MODE_RGB_TEST);
__attribute__ ((weak))
void led_test_init(void) {}

void matrix_scan_user(void) {
     led_test_init();
     iota_gfx_task();  // this is what updates the display continuously
}

void matrix_update(struct CharacterMatrix *dest,
                          const struct CharacterMatrix *source) {
  if (memcmp(dest->display, source->display, sizeof(dest->display))) {
    memcpy(dest->display, source->display, sizeof(dest->display));
    dest->dirty = true;
  }
}

//assign the right code to your layers for OLED display
#define L_COLEMAK 0
#define L_CURSOR (1<<_CURSOR)
#define L_MOUSE (1<<_MOUSE)
#define L_NUMBER (1<<_NUMBER)
#define L_DOUBLE (1<<_NUMBER)|(1<<_DOUBLE)
#define L_ADJUST (1<<_ADJUST)

static void render_logo(struct CharacterMatrix *matrix) {

  static char logo[]={
    0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
    0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
    0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,
    0};
  matrix_write(matrix, logo);
  //matrix_write_P(&matrix, PSTR(" Split keyboard kit"));
}



void render_status(struct CharacterMatrix *matrix) {

  // Render to mode icon
  static char logo[][2][3]={{{0x95,0x96,0},{0xb5,0xb6,0}},{{0x97,0x98,0},{0xb7,0xb8,0}}};
  if(keymap_config.swap_lalt_lgui==false){
    matrix_write(matrix, logo[0][0]);
    matrix_write_P(matrix, PSTR("\n"));
    matrix_write(matrix, logo[0][1]);
  }else{
    matrix_write(matrix, logo[1][0]);
    matrix_write_P(matrix, PSTR("\n"));
    matrix_write(matrix, logo[1][1]);
  }

  // Define layers here, Have not worked out how to have text displayed for each layer. Copy down the number you see and add a case for it below
  if (LAYOUT_STATUS == JP_LAYOUT) {
    matrix_write_P(matrix, PSTR("Colemak JP"));
  } else {
    matrix_write_P(matrix, PSTR("Colemak US"));
  }

  // Host Keyboard LED Status
  char led[40];
    snprintf(led, sizeof(led), "\n%s  %s  %s",
            (host_keyboard_leds() & (1<<USB_LED_NUM_LOCK)) ? "NUMLOCK" : "       ",
            (host_keyboard_leds() & (1<<USB_LED_CAPS_LOCK)) ? "CAPS" : "    ",
            (host_keyboard_leds() & (1<<USB_LED_SCROLL_LOCK)) ? "SCLK" : "    ");
  matrix_write(matrix, led);
}


void iota_gfx_task_user(void) {
  struct CharacterMatrix matrix;

#if DEBUG_TO_SCREEN
  if (debug_enable) {
    return;
  }
#endif

  matrix_clear(&matrix);
  if(is_master){
    render_status(&matrix);
  }else{
    render_logo(&matrix);
  }
  matrix_update(&display, &matrix);
}

#endif
